#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utils import *

def obj_detection_callback(data):
    """
    callback for the wall detection
        - lows[rid] = 'free'
        - lows[rid] = 'left'
        - lows[rid] = 'front'
        - lows[rid] = 'right'
    """
    rid = int(data.header.frame_id[5])		            # number in the robot topic 'm2wr_0' - will not work above 9!!
    low_regions[rid] = {'right': 	data.ranges[0:57],
				        'front': 	data.ranges[58:121],
				        'left':		data.ranges[122:179]}

    min_right = min(low_regions[rid]['right'])
    min_front = min(low_regions[rid]['front'])
    min_left  = min(low_regions[rid]['left'])

    if min_right < SHORT_LOW_DIST or min_front < SHORT_LOW_DIST or min_left < SHORT_LOW_DIST:
        if min_right < min_front and min_right < min_left:
            lows[rid] = 'right'
        elif min_left < min_front and min_left < min_right:
            lows[rid] = 'left'
        else:
            lows[rid] = 'front'
    else:
        lows[rid] = 'free'

def laser_callback(data):
    """
    callback for the wall detection
        - highs[rid] = 'free'
        - highs[rid] = 'wall'
    """
    rid = int(data.header.frame_id[5])	            # number in the robot topic - will not work above 9!!
    high_regions[rid] = {'right': 	data.ranges[30:80],
                         'front': 	data.ranges[70:110],
                         'left':		data.ranges[90:150]}
    min_right = min(high_regions[rid]['right'])
    min_front = min(high_regions[rid]['front'])
    min_left  = min(high_regions[rid]['left'])

    if min_right < SHORT_HIGH_DIST or min_front < SHORT_HIGH_DIST or min_left < SHORT_HIGH_DIST:
	    highs[rid] = 'wall'
    else:
	    highs[rid] = 'free'


def move(rid):
    """
    Function for random movement with wall avoidance
    """
    if highs[rid] == 'free' and keep_turning_flag[rid]==0:
        if DEBUG and rid == 0:
            print("[ INFO] >> move():\tmoving straight")
        vel_msgs[rid].linear.x = SPEED + rid/NUM_ROBOTS
        vel_msgs[rid].angular.z = 0 # (random.random()-0.5)*2*math.pi
    else:
        vel_msgs[rid].linear.x = 0
        keep_turning_flag[rid] = 1
        if highs[rid] =='free':
            keep_turning_counter[rid] +=1
        else:
            keep_turning_counter[rid] = 0
        if keep_turning_counter[rid] > KEEP_TURNING_THRESOLD:
            keep_turning_flag[rid] = 0
        if turn_counters[rid] < TURN_THRESOLD:
            if DEBUG and rid == 0:
                print("[ INFO] >> move():\tturning left\t[{}/{}]".format(turn_counters[rid],TURN_THRESOLD))
            vel_msgs[rid].angular.z = LEFT
            turn_counters[rid] += 1
        else:
            if DEBUG and rid == 0:
                print("[ INFO] >> move():\tturning right[{}/{}]".format(turn_counters[rid],2*TURN_THRESOLD))
            vel_msgs[rid].angular.z = RIGHT
            turn_counters[rid] += 1
            if turn_counters[rid] == 2*TURN_THRESOLD:
                turn_counters[rid] = 0


def push_can(rid):
    """
    Function for pushing a can to a wall
        - follow the can until is in the front
        - push it
    """
    if lows[rid] == 'left':
        if DEBUG and rid == 0:
            print("[ INFO] >> push_can():\tturning left")
        vel_msgs[rid].linear.x = 0
        vel_msgs[rid].angular.z = LEFT
    elif lows[rid] == 'right':
        if DEBUG and rid == 0:
            print("[ INFO] >> push_can():\tturning right")
        vel_msgs[rid].linear.x = 0
        vel_msgs[rid].angular.z = RIGHT
    elif lows[rid] == 'front':
        if DEBUG and rid == 0:
            print("[ INFO] >> push_can():\tpushing can")
        vel_msgs[rid].linear.x = SPEED - rid*0.1
        vel_msgs[rid].angular.z = 0
    else:
        if DEBUG and rid == 0:
            print("[ INFO] >>   don't worry - be happy")


def step_back(rid):
    """
    Function for stepping back after having pushed a can to a wall
        - step back for a while
        - turn 180°
    """
    if states[rid] == 'default':
        states[rid] = 'recover'
        stepback_counters[rid] = 0
        if min(high_regions[rid]['right']) < min(high_regions[rid]['left']):
            turn_flags[rid] = LEFT
        else:
            turn_flags[rid] = RIGHT
    if stepback_counters[rid] < BACK_THRESOLD:
        vel_msgs[rid].linear.x= -SPEED
        vel_msgs[rid].angular.z = 0
        stepback_counters[rid] += 1
        if DEBUG and rid == 0:
            print("[ INFO] >> step_back():\tstepping back\t[{}/{}]".format(stepback_counters[rid],BACK_THRESOLD))
    else:
        if stepback_counters[rid] < 2*BACK_THRESOLD:
            vel_msgs[rid].linear.x= 0
            vel_msgs[rid].angular.z = turn_flags[rid]
            stepback_counters[rid] += 1
            if DEBUG and rid == 0:
                print("[ INFO] >> step_back():\tturning right[{}/{}]".format(stepback_counters[rid],2*BACK_THRESOLD))
        else:
            states[rid] = 'default'


### --------------- MAIN --------------- ###


def main():
    global vel_pubs
    global vel_msgs
    global lows
    global highs
    global states
    global turn_flags
    global low_regions
    global high_regions
    global turn_counters
    global stepback_counters
    global keep_turning_counter
    global keep_turning_flag

    rospy.init_node('crazy_robot_cleaner', anonymous=True)

    vel_pubs   = []             # array of publishers
    vel_msgs   = []             # array of subscribers

    lows   = []                 # variable for storing if a low object has been detected or not
    highs  = []                 # variable for storing if an upper object has been detected or not
    states = []				    # variable to know in which state the step_back function is

    turn_flags   = []           # variable to know, before starting the recovery, which wall is the closest, in order to avoid it
    low_regions  = []           # variable to store the low laser data
    high_regions = []           # variable to store the high laser data

    turn_counters     = []      # variable for changing the circling direction
    stepback_counters = []      # variable for knowing when to stop stepping back

    keep_turning_flag = []		#flag to know if the robot has to keep turning so its not parallel to a wall
    keep_turning_counter = []   #variable to know if the robot has to keep turning so its not parallel to a wall

    for i in range(0, NUM_ROBOTS):
        lows.append('free')
        highs.append('free')
        states.append('default')

        turn_flags.append(LEFT)
        low_regions.append(None)
        high_regions.append(None)

        turn_counters.append(0)
        stepback_counters.append(0)

        keep_turning_flag.append(0)
        keep_turning_counter.append(0)

        vel_msg = Twist()
        vel_msgs.append(vel_msg)
        vel_pubs.append(rospy.Publisher('/m2wr_'+str(i)+'/cmd_vel', Twist, queue_size=10))

        rospy.Subscriber('/m2wr_'+str(i)+'/laser_down', LaserScan, obj_detection_callback)	# for low objects
        rospy.Subscriber('/m2wr_'+str(i)+'/laser_up',   LaserScan, laser_callback)			# for high objects
        print(">> Robot R{} ready to work.".format(i))

    while True:
        for rid in range(0, NUM_ROBOTS):
            if states[rid] == 'recover':  	#if its stepping back keep steping back
                step_back(rid)
            else:							#else check envoronment
                if lows[rid] == 'free':			#if low sensor sees nothing, keep moving
                    move(rid)
                else:							#else push or step back
                    if highs[rid] == 'free':		#if there is no wall close push
                        push_can(rid)
                    else:							#if there is a wall close already, step back
                        if lows[rid]=='front':
                            step_back(rid)
                        else:
                            move(rid)
            vel_pubs[rid].publish(vel_msgs[rid])
        rospy.sleep(0.1)                            # update ratio: 10 Hz


if __name__ == '__main__':
	try:
		main()
	except rospy.ROSInterruptException: pass

'''
############ TO DO LIST ################

~.  Narrow center range for detecting objects
2.  Use a different thresold for detecting objects, much more longer, otherwise they will wander too much randomly
3.  Try with more robots


X.  Write the report
X.  Record the video

'''


