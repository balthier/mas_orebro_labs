import rospy, math, random
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan

SPEED = 0.8				# default speed of the robot
RIGHT = 3*SPEED			# value for moving right in angular.z
LEFT = -RIGHT			# value for moving left in angular.z
SHORT_LOW_DIST = 0.4		# below this distance, a low object is detected
LONG_LOW_DIST = 2
SHORT_HIGH_DIST = 0.9		# below this distance, a high object is detected
LONG_HIGH_DIST = 3
NUM_ROBOTS = 3			# numbers of robot in the simulation --> change also spawn.launch!
DEBUG = 0				# prints additional infos
TURN_THRESOLD = 200		# number of iterations that have to pass before the robot changes its circling direction
BACK_THRESOLD = 15		# number of iterations for stepping back and turning around after having pushed a can
KEEP_TURNING_THRESOLD = 20 # number of iterations to keep turning after seeing a wall so it doesn't go parallel to it

vel_pubs = None
vel_msgs = None
