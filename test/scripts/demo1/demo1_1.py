#!/usr/bin/env python
from utils import *


def obj_detection_callback(data):
	rid = int(data.header.frame_id[5])		# number in the robot topic 'm2wr_0' - will not work above 9!!
	if min(data.ranges[70:110]) < SHORT_LOW_DIST:
		low_flags[rid] = 1
	else:
		low_flags[rid] = 0


def laser_callback(data):
	rid = int(data.header.frame_id[5])	# number in the robot topic - will not work above 9!!
	regions = {	'right': 	data.ranges[40:80],
				'front': 	data.ranges[70:110],
				'left':		data.ranges[90:140]}

	if min(regions['right']) < SHORT_HIGH_DIST or min(regions['front']) < SHORT_HIGH_DIST or min(regions['left']) < SHORT_HIGH_DIST:
		high_flags[rid] = 1
	else:
		high_flags[rid] = 0

	if high_flags[rid] == 0 and low_flags[rid] == 1:
		vel_msgs[rid].linear.x = SPEED/2
		print("Pushing")
	elif high_flags[rid] == 1 and low_flags[rid] == 1:
		vel_msgs[rid].linear.x = 0
		print("Done pushing - Wall reached")
	else:
		move(rid, regions)
	if rid == 0:
		print("---LOWS--|--HIGHS---")
		print("{} {}".format(low_flags, high_flags))

	vel_pubs[rid].publish(vel_msgs[rid])


def move(rid, regions):
	if min(regions['front']) > SHORT_HIGH_DIST and min(regions['right']) > SHORT_HIGH_DIST and min(regions['left']) > SHORT_HIGH_DIST:
		vel_msgs[rid].linear.x = SPEED
		vel_msgs[rid].angular.z = 0 				#(random.random()-0.5)*2*math.pi

	else:											# movement in left or right
		vel_msgs[rid].linear.x = 0
		if counter[0] < TURN_THRESOLD:
			vel_msgs[rid].angular.z = LEFT
			counter[0] += 1
		else:
			vel_msgs[rid].angular.z = RIGHT
			counter[0] += 1
			if counter[0] == 2*TURN_THRESOLD:
				counter[0] = 0


def main():
	global vel_pubs
	global vel_msgs
	global counter
	global low_flags
	global high_flags

	rospy.init_node('crazy_robot_cleaner', anonymous=True)

	low_flags  = []		# variable for storing if a low object has been detected or not
	high_flags = []		# variable for storing if an upper object has been detected or not
	counter   = [0]				# variable for changing the circling direction
	vel_pubs   = []
	vel_msgs   = []

	for i in range(0, NUM_ROBOTS):
		low_flags.append(0)
		high_flags.append(0)

		vel_msg = Twist()
		vel_msgs.append(vel_msg)

		vel_pubs.append(rospy.Publisher('/m2wr_'+str(i)+'/cmd_vel', Twist, queue_size=10))

		rospy.Subscriber('/m2wr_'+str(i)+'/laser_down', LaserScan, obj_detection_callback)	# for low objects
		rospy.Subscriber('/m2wr_'+str(i)+'/laser_up', LaserScan, laser_callback)			# for high objects
		print(">> Robot R{} ready to work.".format(i))
		print(vel_msgs[i])

	rospy.spin()


if __name__ == '__main__':
	try:
		main()
	except rospy.ROSInterruptException: pass
