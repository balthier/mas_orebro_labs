# -*- coding: utf-8 -*-

from abstract_robot import *

class SearcherRobot(Robot):
    """
    Class for storing the searcher robot infos
    """
    def __init__(self, rid, pusher_robots):
        Robot.__init__(self, rid)
        self.robot_state = WORKING
        self.helper_robot = 0

        for i in range(0, NUM_PUSHERS+NUM_SEARCHERS):
            if i != self.rid:
                #print("[ INFO] >> R{} is subscribing to /m2wr_{}/avoidance topic".format(rid, i))
                #rospy.Subscriber('/m2wr_'+str(i)+'/string', String, self.avoid_callback)
                #if i in pusher_robots:
                print("[ INFO] >> R{} is subscribing to /m2wr_{}/string topic".format(rid, i))
                rospy.Subscriber('/m2wr_'+str(i)+'/string', String, self.searcher_help_callback)

        print("        \t>> R{}: search robot ready to work".format(rid))

############ CALLBACKS ############

    def searcher_help_callback(self, data):
        """
        reads the messages published by the pusher robot(s)
        - 'going x/y <value>':  published when the pusher robot is moving straight
        - 'finished':           published when the pusher robot is done
        """
        tmp = data.data.split()
        if tmp[0] == 'finished':
            self.robot_state = WORKING

        elif tmp[0] == 'going':
            if tmp[1] == 'x':
                if self.odom_data.position.x > float(tmp[2]) - EPS and self.odom_data.position.x < float(tmp[2]) + EPS:
                    if self.odom_data.position.x < 0:
                        self.w = 0
                    else:
                        self.w = math.pi
                    print("[ INFO] >> R{} will move because he is at x {}".format(self.rid, self.odom_data.position.x))
                    self.robot_state = AVOIDING
                else:
                    self.str_pub.publish("clear")
                    print("[ INFO] >> R{}: way already cleared".format(self.rid))

            else:
                if self.odom_data.position.y > float(tmp[2]) - EPS and self.odom_data.position.y < float(tmp[2]) + EPS:
                    if self.odom_data.position.y < 0:
                        self.w = math.pi/2
                    else:
                        self.w = 3*math.pi/2
                    print("[ INFO] >> R{} will move because he is at y {}".format(self.rid, self.odom_data.position.y))
                    self.robot_state = AVOIDING
                else:
                    self.str_pub.publish("clear")
                    print("[ INFO] >> R{}: way already cleared".format(self.rid))

        elif tmp[0] == 'clear' or tmp[0] == 'dist':
            pass

        else:
            print("[ ERR] >> searcher_help_callback: state communication error : data = {}".format(data))

    def ask_help(self):
        my_theta=2*math.acos(self.odom_data.orientation.w)
        if self.odom_data.orientation.z < 0:
            my_theta=2*math.pi - my_theta
        # get x and y
        x = self.odom_data.position.x + 0.25*math.cos(my_theta)
        y = self.odom_data.position.y + 0.25*math.sin(my_theta)
        if x < WEST_WALL - 0.2 or x > EAST_WALL + 0.2 or y < SOUTH_WALL - 0.2 or y > NORTH_WALL + 0.2:
            self.robot_state = WORKING
            return

        s = 'help '+str(x)+' '+str(y)

        print("[ INFO] >> R{} asks for help at: ({})".format(self.rid, s))
        self.str_pub.publish(s)

############ MOVEMENT #############

    def search_can(self):
        """
        Random movement with collision avoidance
            - walk straight for a while
                - if there's an object, turn to avoid
            - after some time, choose one turning direction
            - turn for some time (maybe random (?))
            - repeat
        """
        self.action1 = "searching can"
        if self.straight < KEEP_TURNING_THRESHOLD/2:
            self.turning_dir = None
            if self.short_high == 'free':
                self.move_straight()
                self.straight += 1
            else:
                self.wait()
                self.straight = KEEP_TURNING_THRESHOLD/2

        elif self.straight == KEEP_TURNING_THRESHOLD/2:
            #print(self.high_regions.keys())
            if min(self.high_regions["left"]) < min(self.high_regions["right"]):
                self.turning_dir = RIGHT
            else:
                self.turning_dir = LEFT
            self.straight += 1

        elif self.straight > KEEP_TURNING_THRESHOLD/2 and self.straight < KEEP_TURNING_THRESHOLD:
            self.turn()
            self.straight += 1

        else:
            self.turning_dir = None
            self.straight = 0

    def reach_can(self):
        """
        Turns the robot to make it face the can
        """
        self.action1 = "reaching can"
        if self.long_low == 'left':
            self.turning_dir = LEFT
        elif self.long_low == 'right':
            self.turning_dir = RIGHT
        else:
            self.turning_dir = None
        self.turn()

    def push_can(self):
        """
        Pushes the can to a wall and tries to avoid other robots
        """
        self.action1 = "pushing can"
        #if self.short_high == 'free':
        if min(self.high_regions["front"]) > 0.5:
            self.turning_dir = None
            self.move_straight()
        else:
            wall_near = self.is_wall(radius=0.5)
            if wall_near == None:
                if self.turning_dir == None:
                    if min(self.high_regions["left"]) < min(self.high_regions["right"]):
                        self.turning_dir = RIGHT
                    else:
                        self.turning_dir = LEFT
                self.turn()
            else:
                self.turning_dir = None
                self.move_straight()

    def avoid_subroutine(self):
        """
        Makes the robot avoid another robot and go away from the walls
        """
        if self.avoiding_flag == 5:
            self.avoiding_flag = 0
        else:
            if self.avoiding_counter == 0:
                self.choose_avoiding_direction()
                self.avoiding_counter = 1
                self.wait()
            elif self.avoiding_counter > 0 and self.avoiding_counter < AVOIDING_THRESHOLD:
                self.move_straight(turning=True)
                self.avoiding_counter += 1
            else:
                self.backward = False
                self.turning_dir = None
                self.avoiding_counter = 0
                if self.is_wall() == None:
                    self.str_pub.publish("clear")
                    print("[ INFO] >> R{}: way cleared".format(self.rid))
                    self.robot_state = WAITING
                else:
                    self.avoiding_flag += 1
                    self.robot_state = AVOIDING

    def choose_turning_dir(self):
        """
        Choose the turning dir to make the robot avoid the wall while going backward
        """
        self.action1 = "choosing turn dir"
        if self.short_high == 'right':
            self.action2 = "left!"
            self.turning_dir = LEFT
        elif self.short_high == 'left':       # it was the opposite before
            self.action2 = "right!"
            self.turning_dir = RIGHT
        else:
            self.turning_dir = None

    def print_info(self):
        sl = self.short_low
        ll = self.long_low
        sh = self.short_high
        lh = self.long_high

        if self.is_wall() == NORTH_WALL:
            wall = "North"
        elif self.is_wall() == SOUTH_WALL:
            wall = "South"
        elif self.is_wall() == EAST_WALL:
            wall = "East"
        elif self.is_wall() == WEST_WALL:
            wall = "West"
        else:
            wall = "None"

        s = "R%s (searcher) %s: %s\t(%s)\t%s\t%s\t%s\t%s\t%s" %(self.rid, self.robot_state, self.action1, self.action2, sl, ll, sh, lh, wall)

        if s != self.info_string:
            print(s)
            self.info_string = s