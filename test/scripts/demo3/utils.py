# -*- coding: utf-8 -*-
import rospy, math, random
from std_msgs.msg       import String
from geometry_msgs.msg  import Twist
from nav_msgs.msg       import Odometry
from sensor_msgs.msg    import LaserScan

######################################################## COSTANTS ########################################################

DEBUG         = False			    # prints debug's info
INFO         = True                # prints robot's info
NUM_PUSHERS   = 2			        # numbers of pusher robots in the simulation --> change also spawn.launch!
NUM_SEARCHERS = 1                   # numbers of searcher robots in the simulation --> change also spawn.launch!
INFINITE      = 1000                # infinite distance for the distance array

SPEED = 0.8				        # default speed of the robot
RIGHT = 3*SPEED			    # value for moving right in angular.z
LEFT  = -RIGHT			        # value for moving left in angular.z
EPS   = 0.5

SHORT_LOW_DIST  = 0.4		    # below this distance, a low object is detected
LONG_LOW_DIST   = 2
SHORT_HIGH_DIST = 0.9		    # below this distance, a high object is detected
LONG_HIGH_DIST  = 3

TURN_THRESHOLD         = 200	# number of iterations that have to pass before the robot changes its circling direction
BACK_THRESHOLD         = 5		# number of iterations for stepping back and turning around after having pushed a can
KEEP_TURNING_THRESHOLD = 20     # number of iterations to keep turning after seeing a wall so it doesn't go parallel to it
AVOIDING_THRESHOLD     = 5

NORTH_WALL  =  7.5              # y coordinate of the North Wall
SOUTH_WALL  = -7.5                # y coordinate of the South Wall
EAST_WALL   =  5.8              # x coordinate of the East Wall
WEST_WALL   = -5.8              # x coordinate od the West Wall

### TURN VALUES ###
NORTH_W = math.pi/2
SOUTH_W = 3*math.pi/2
EAST_W = 0.0
WEST_W = math.pi


STAGE1_TURN = 'stage1_turn'
STAGE1_REACH =  'stage1_reach'
STAGE2_TURN =    'stage2_turn'
STAGE2_REACH = 'stage2_reach'
STAGE3 = 'stage3'
STAGE4_TURN = 'stage4_turn'
STAGE4_REACH = 'stage4_reach'
######################################################## GLOBALS ########################################################


global distances
global availability

distances = []                  # array of distances between pusher robots and goal
availability = []               # boolean array to know which robot is free



######################################################## UTILS FUNC ########################################################



def avg(data):
    """
    Computes the average of an iterable data
    """
    return sum(data)/len(data)
