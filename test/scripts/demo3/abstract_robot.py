# -*- coding: utf-8 -*-

from utils import *

### COMMON STATES ###
WAITING     = 'WAITING'
AVOIDING    = 'AVOIDING'

### SEARCHER STATES ###
WORKING     = 'WORKING'

### PUSHER   STATES ###
FACE_WALL   = 'FACING WALL'
REACH_WALL  = 'REACHING WALL'
FACE_CAN    = 'FACING CAN'
PUSH_CAN    = 'PUSHING CAN'
STEPBACK    = 'STEPPING BACK'
DECIDING    = 'DECIDING'

### STEPPING BACK STATES ###
DEFAULT  = 'DEFAULT'
BACKWARD = 'BACKWARD'

class Robot:
    """
    Abstract class for storing all the common variables of the robots and the common methods
    """
    def __init__(self, rid):
        self.rid = rid
        self.info_string = ""
        self.state = DEFAULT           # FOR STEPPING BACK
        self.odom_data  = None
        self.w = None
        self.angle = 0

        self.long_low     = 'free'
        self.short_low    = 'free'
        self.long_high    = 'free'
        self.short_high   = 'free'
        self.low_regions  =  {}
        self.high_regions =  {}

        self.turn_flag            = LEFT
        self.turn_counter         = 0
        self.stepback_counter     = 0
        self.keep_turning_flag    = 0
        self.keep_turning_counter = 0

        self.straight = 0
        self.random = 0
        self.random_time = 0

        self.vel_msg = Twist()
        self.str_msg = String()
        self.avoid_msg = String()
        self.vel_pub = rospy.Publisher('/m2wr_'+str(rid)+'/cmd_vel', Twist,  queue_size=20)
        self.str_pub = rospy.Publisher('/m2wr_'+str(rid)+'/string',  String, queue_size=20)

        self.turning_dir = None
        self.backward    = False
        self.avoiding_counter = 0
        self.stepback_flag = False

        self.robot_state  = None
        self.action1 = None
        self.action2 = None

        self.avoiding_flag = 0
        self.clearway_flag = 0

        rospy.Subscriber('/m2wr_'+str(rid)+'/odom',       Odometry,  self.odom_callback)
        rospy.Subscriber('/m2wr_'+str(rid)+'/laser_down', LaserScan, self.obj_detection_callback)	# for low objects
        rospy.Subscriber('/m2wr_'+str(rid)+'/laser_up',   LaserScan, self.laser_callback)			# for high objects

############ CALLBACKS ###############

    def odom_callback(self, data):
        """
        callback for the odom data
        - pose.pose.position:       (x,y,z)
        - pose.pose.orientation:    (x,y,z,w)
        """
        rid = int(data.header.frame_id[5])		            # number in the robot topic 'm2wr_0' - will not work above 9!!
        if rid == self.rid:
            self.odom_data = data.pose.pose

    def obj_detection_callback(self, data):
        """
        callback for the wall detection, 4 possible string values for `self.short_low` and `self.long_low`:
            - 'free'
            - 'left'
            - 'front'
            - 'right'
        """
        rid = int(data.header.frame_id[5])		            # number in the robot topic 'm2wr_0' - will not work above 9!!
        if rid == self.rid:
            self.low_regions = {'right': 	data.ranges [0:79],     #[0:59],
                                'front': 	data.ranges [80:100],    #[60:119], 20° ehere is perfect with old robots
                                'left':		data.ranges [101:179]}  #[120:179]}

            min_right = min(self.low_regions['right'])
            min_front = min(self.low_regions['front'])
            min_left  = min(self.low_regions['left'])

            if min(min_left, min_right, min_front) < LONG_LOW_DIST:
                if min_right < min_front and min_right < min_left:
                    self.long_low = 'right'
                    if min_right < SHORT_LOW_DIST:
                        self.short_low = 'right'
                elif min_left < min_front and min_left < min_right:
                    self.long_low = 'left'
                    if min_left < SHORT_LOW_DIST:
                        self.short_low = 'left'
                else:
                    self.long_low = 'front'
                    if min_front < SHORT_LOW_DIST:
                        self.short_low = 'front'
            else:
                self.long_low  = 'free'
                self.short_low = 'free'

    def laser_callback(self, data):
        """
        callback for the wall detection
            - short_highs = 'free'
            - short_highs = 'wall'
        """
        rid = int(data.header.frame_id[5])	            # number in the robot topic - will not work above 9!!
        if rid == self.rid:
            self.high_regions = {'right': 	data.ranges[0:59],      #{'right': 	data.ranges[30:80],
                                'front': 	data.ranges[60:119],    # 'front': 	data.ranges[70:110],
                                'left':		data.ranges[120:179]}    # 'left':	data.ranges[90:150]}

            min_right = min(self.high_regions['right'])
            min_front = min(self.high_regions['front'])
            min_left  = min(self.high_regions['left'])

            if min(min_left, min_front, min_right) < LONG_HIGH_DIST:
                if min_right < min_front and min_right < min_left:
                    self.long_high = 'right'
                    if min_right < SHORT_HIGH_DIST + 0.3:
                        self.short_high = 'right'
                elif min_left < min_front and min_left < min_right:
                    self.long_high = 'left'
                    if min_left < SHORT_HIGH_DIST + 0.3:
                        self.short_high = 'left'
                else:
                    self.long_high = 'front'
                    if min_front < SHORT_HIGH_DIST + 0.3:
                        self.short_high = 'front'
            else:
                self.long_high  = 'free'
                self.short_high = 'free'

    def avoid_callback(self, data):
        print("R{} received: {}".format(self.rid, data))
        tmp = data.data.split()
        if tmp[0] == 'going':
            if tmp[1] == 'x':
                if self.odom_data.position.x > float(tmp[2]) - EPS and self.odom_data.position.x < float(tmp[2]) + EPS:
                    if self.odom_data.position.x < 0:
                        self.w = EAST_W
                    else:
                        self.w = WEST_W
                    print("[ INFO] >> R{} will move because he is at x {}".format(self.rid, self.odom_data.position.x))
                    self.robot_state = AVOIDING
                else:
                    self.str_pub.publish("clear")
                    print("[ INFO] >> R{}: way already cleared".format(self.rid))

            else:
                if self.odom_data.position.y > float(tmp[2]) - EPS and self.odom_data.position.y < float(tmp[2]) + EPS:
                    if self.odom_data.position.y < 0:
                        self.w = NORTH_W
                    else:
                        self.w = SOUTH_W
                    print("[ INFO] >> R{} will move because he is at y {}".format(self.rid, self.odom_data.position.y))
                    self.robot_state = AVOIDING
                else:
                    self.str_pub.publish("clear")
                    print("[ INFO] >> R{}: way already cleared".format(self.rid))

        elif tmp[0] == 'clear':
            self.clearway_flag += 1
            print("ok")
        elif tmp[0] == 'help':
            pass
        else:
            print("[ ERR] >> avoid_callback: state communication error : data = {}".format(data))

############ COMMONS #################

    def wait(self):
        """
        Stops the robot
        """
        self.action2 = "waiting"
        self.vel_msg.linear.x  = 0
        self.vel_msg.angular.z = 0

    '''def move(self):
        """
        Function for random movement with wall avoidance
        """
        self.action2 = "random movement"
        if self.random_time == KEEP_TURNING_THRESHOLD:
            self.random_time = 0
            self.random = (random.random()-0.5)*4*math.pi

        self.straight += 1
        self.random_time += 1

        if  self.short_high == 'free' and self.keep_turning_flag == 0 and \
            self.straight < 0.5*TURN_THRESHOLD:
            if DEBUG:
                print("[ INFO] >> move():\tmoving straight")
            self.vel_msg.linear.x  = SPEED
            self.vel_msg.angular.z = self.random
        else:
            self.vel_msg.linear.x = 0
            self.keep_turning_flag = 1

            if self.short_high =='free':
                self.keep_turning_counter +=1
            else:
                self.keep_turning_counter = 0

            if self.keep_turning_counter > KEEP_TURNING_THRESHOLD:
                self.keep_turning_flag = 0

            if self.turn_counter < TURN_THRESHOLD:
                if DEBUG:
                    print("[ INFO] >> move():\tturning left\t[{}/{}]".format(self.turn_counter, TURN_THRESHOLD))
                self.vel_msg.angular.z = LEFT
                self.turn_counter += 1

            else:
                if DEBUG:
                    print("[ INFO] >> move():\tturning right[{}/{}]".format(self.turn_counter, 2*TURN_THRESHOLD))
                self.vel_msg.angular.z = RIGHT
                self.turn_counter += 1

                if self.turn_counter == 2*TURN_THRESHOLD:
                    self.turn_counter = 0

            if self.straight == TURN_THRESHOLD:
                self.straight = 0
        '''

    def turn(self):
        """
        Makes the robot turn in the `self.turning_dir`, if any, otherwise it goes straight
        """
        if self.turning_dir == None:
            self.action2 = "moving straight"
            self.vel_msg.linear.x = SPEED
            self.vel_msg.angular.z = 0
        else:
            if self.turning_dir == LEFT:
                self.action2 = "turning left   "
            else:
                self.action2 = "turning right  "
            self.vel_msg.linear.x = 0
            self.vel_msg.angular.z = self.turning_dir

    def step_back(self):
        """
        Function for stepping back after having pushed a can to a wall
            - step back for a while
            - turn 180°
        """
        self.straight = 0
        if self.state == DEFAULT:
            self.state = BACKWARD
            self.stepback_counter = 0
            if min(self.high_regions['right']) < min(self.high_regions['left']):
                self.turning_dir = LEFT
            else:
                self.turning_dir = RIGHT

        if self.stepback_counter < BACK_THRESHOLD:
            self.backward = True
            self.move_straight()
            self.stepback_counter += 1
            if DEBUG:
                print("[ INFO] >> step_back():\tstepping back\t[{}/{}]".format(self.stepback_counter, BACK_THRESHOLD))
        elif self.stepback_counter > BACK_THRESHOLD and self.stepback_counter < 2*BACK_THRESHOLD:
                self.backward = False
                self.turn()
                self.stepback_counter += 1
                if DEBUG:
                    print("[ INFO] >> step_back():\tturning [{}/{}]".format(self.stepback_counter, 2*BACK_THRESHOLD))
        else:
            self.state = DEFAULT

    def move_straight(self, turning=False):
        if self.backward == True:
            s = "moving backward"
            self.vel_msg.linear.x = -SPEED
        else:
            s = "moving straight"
            self.vel_msg.linear.x = SPEED
        if turning == True and self.turning_dir != None:
            if self.turning_dir == LEFT:
                s = s + "-left"
            else:
                s = s + "-right"
            self.vel_msg.angular.z = self.turning_dir
        else:
            self.vel_msg.angular.z = 0
        self.action2 = s

    def get_mins(self):
        """
        Returns the minimum of low and high values
        """
        low_min = min(  min(self.high_regions['right']),
                        min(self.high_regions['front']),
                        min(self.high_regions['left']))

        high_min = min( min(self.high_regions['right']),
                        min(self.high_regions['front']),
                        min(self.high_regions['left']))
        return low_min, high_min

    def choose_avoiding_direction(self):
        """
        Chooses the avoiding direction when asked to move
        - if close to wall:
            - if wall to the front,     go backward
            - elif wall to the left,    go right
            - elif wall to the right,   go left
        - else:
            - go backward
        """
        self.action1 = "choosing avoiding direction"
        if self.avoiding_flag < 4:
            if self.is_wall() != None:
                low_min, high_min = self.get_mins()
                if (avg(self.high_regions['front']) > 0.5 and min(self.low_regions['front']) > 0.5) or \
                (avg(self.high_regions['front']) > 0.5+0.24 and min(self.low_regions['front']) < 0.25):
                #if min(self.high_regions['front']) < 0.35:
                    self.backward = True
                    if min(self.high_regions['left']) < min(self.high_regions['right']) and min(self.high_regions['left']) > 0.1:
                        self.turning_dir = RIGHT
                    elif min(self.high_regions['right']) < min(self.high_regions['left']) and min(self.high_regions['right']) > 0.1:
                        self.turning_dir = LEFT
                    else:
                        self.turning_dir = None
                else:
                    if min(self.high_regions['left']) == high_min:
                        self.backward = False
                        self.turning_dir = RIGHT
                    else:
                        self.backward = False
                        self.turning_dir = LEFT
            else:
                self.turning_dir = None
                self.backward = True
        else:
            if self.is_wall() != None:
                self.backward = False
                if self.is_wall()==NORTH_WALL:
                #head south
                    self.w= SOUTH_W
                if self.is_wall()==SOUTH_WALL:
                #head north
                    self.w = NORTH_W
                if self.is_wall()== EAST_WALL:
                #head west
                    self.w = WEST_W
                if self.is_wall()== WEST_WALL:
                #head east
                    self.w = EAST_W
                my_theta = 2*math.acos(self.odom_data.orientation.w)
                if self.odom_data.orientation.z < 0:
                    my_theta=2*math.pi - my_theta
                self.angle = self.w - my_theta
                if self.angle > math.pi:
                    self.angle = self.angle-2*math.pi
                if self.angle < -math.pi:
                    self.angle = 2*math.pi+self.angle
                if self.angle > 0:
                    self.turning_dir= LEFT
                else:
                    self.turning_dir= RIGHT
            else:
                self.turning_dir = None
                self.backward = True

    def is_wall(self, radius=1.5*EPS):
        """
        Returns the closest wall to a robot, in a range of EPS = 0.5
        """
        if self.odom_data != None:
            x = self.odom_data.position.x
            y = self.odom_data.position.y
            if y > NORTH_WALL - radius:
                return NORTH_WALL
            elif y < SOUTH_WALL + radius:
                return SOUTH_WALL
            else:
                if x > EAST_WALL - radius:
                    return EAST_WALL
                elif x < WEST_WALL + radius:
                    return WEST_WALL
                else:
                    return None
        return None