#!/usr/bin/env python
# -*- coding: utf-8 -*-

from utils import *
from pusher_robot import *

def pusher_routine(rid):
    eps=0.2
    sl = rid.short_low
    ll = rid.long_low
    sh = rid.short_high
    lh = rid.long_high
    my_theta= None
    #stage0 --> initialize behaviour
    #stage1 --> reach goal (can)
    #stage2 --> push can to corner
    if rid.pusher_state == HELPING:
        if EAST_WALL - abs(rid.goal['x']) < NORTH_WALL - abs(rid.goal['y']):    # move horizontally
            if rid.goal['x'] > 0:       # move east
                #rid.w = 0
                rid.goal_state = EAST_WALL
            else:                       # move west
                #rid.w = math.pi
                rid.goal_state = WEST_WALL
            #if rid.odom_data.position.x > rid.goal['x']-eps*2 and rid.odom_data.position.x < rid.goal['x']+ eps*2:
                #   rid.pusher_state = TURNING
        else:                                                                   # move vertically
            if rid.goal['y'] > 0:       # move north
                #rid.w = math.pi/2
                rid.goal_state = NORTH_WALL
            else:                       # move south
                #rid.w= 3*math.pi/2
                rid.goal_state = SOUTH_WALL
            #if rid.odom_data.position.y < rid.goal['y']+ eps*2 and rid.odom_data.position.y > rid.goal['y'] - eps*2:
                #   rid.pusher_state = TURNING
        rid.pusher_state = BACKWARD
    elif rid.pusher_state== STAGE1_TURN:
        rid.vel_msg.linear.x = 0
        rid.wall = rid.is_wall()
        if rid.obstacle == True:
            if rid.prev_w == NORTH_W or rid.prev_w == SOUTH_W:
                if rid.odom_data.position.x < rid.goal['x']:
                    rid.w= EAST_W
                else:
                    rid.w= WEST_W
            else: # rid.goal_state== EAST_WALL or rid.goal_state== WEST_WALL:
                if rid.odom_data.position.y < rid.goal['y']:
                    rid.w= NORTH_W
                else:
                    rid.w= SOUTH_W
        else:
            if rid.wall != None and rid.wall != rid.goal_state:
                if rid.wall==NORTH_WALL:
                #head south
                    rid.w= SOUTH_W
                if rid.wall==SOUTH_WALL:
                #head north
                    rid.w = NORTH_W
                if rid.wall== EAST_WALL:
                #head west
                    rid.w = WEST_W
                if rid.wall== WEST_WALL:
                #head east
                    rid.w = EAST_W
            else:
                #if rid.obstacle == False:
                if rid.goal_state== NORTH_WALL or rid.goal_state== SOUTH_WALL:
                    #HEAD NORTH_WALL
                    if rid.odom_data.position.y < rid.goal['y']:
                        rid.w= NORTH_W
                    else:
                        rid.w=SOUTH_W
                elif rid.goal_state== EAST_WALL or rid.goal_state== WEST_WALL:
                    #HEAD EAST_WALL
                    if rid.odom_data.position.x < rid.goal['x']:
                        rid.w= EAST_W
                    else:
                        rid.w= WEST_W
        my_theta = 2*math.acos(rid.odom_data.orientation.w)
        if rid.odom_data.orientation.z < 0:
            my_theta=2*math.pi - my_theta
        rid.angle = rid.w - my_theta
        if rid.angle > math.pi:
            rid.angle = rid.angle-2*math.pi
        if rid.angle < -math.pi:
            rid.angle = 2*math.pi+rid.angle
        if rid.angle < eps and  rid.angle > -eps:
            #if rid.clearway_flag == 'clear'
            rid.pusher_state= STAGE1_REACH
            #else:
            #   rid.vel_msg.linear.x = 0
            #   rid.vel_msg.angular.z = 0
        else:
            rid.turn_90()
    elif rid.pusher_state == STAGE1_REACH:
        rid.vel_msg.angular.z = 0
        rid.reach_counter += 1
        if (rid.goal_state == NORTH_WALL or rid.goal_state == SOUTH_WALL) and \
            rid.odom_data.position.y < rid.goal['y']+eps and rid.odom_data.position.y > rid.goal['y']-eps:
                rid.pusher_state = STAGE2_TURN
        elif (rid.goal_state == EAST_WALL or rid.goal_state == WEST_WALL) and \
            rid.odom_data.position.x < rid.goal['x']+eps and rid.odom_data.position.x > rid.goal['x']-eps:
                rid.pusher_state = STAGE2_TURN
        else:
            if rid.reach_counter > PUSHER_REACH_THRESHOLD:
                rid.reach_counter = 0
                rid.pusher_state = STAGE1_TURN
            else:
                if sh == 'front':
                    rid.vel_msg.linear.x = 0
                    rid.obstacle= True
                    rid.reach_counter = 0
                    rid.pusher_state = STAGE1_TURN
                    rid.prev_w = rid.w
                else:
                    rid.obstacle = False
                    rid.move_straight()
    elif rid.pusher_state == STAGE2_TURN:
        if rid.goal_state== NORTH_WALL or rid.goal_state== SOUTH_WALL:
                    #HEAD NORTH_WALL
            if rid.odom_data.position.x < rid.goal['x']:
                rid.w= EAST_W
            else:
                rid.w=WEST_W
        elif rid.goal_state== EAST_WALL or rid.goal_state== WEST_WALL:
                    #HEAD EAST_WALL
            if rid.odom_data.position.y < rid.goal['y']:
                rid.w= NORTH_W
            else:
                rid.w= SOUTH_W
        my_theta = 2*math.acos(rid.odom_data.orientation.w)
        if rid.odom_data.orientation.z < 0:
            my_theta=2*math.pi - my_theta
        rid.angle = rid.w - my_theta
        if rid.angle > math.pi:
            rid.angle = rid.angle-2*math.pi
        if rid.angle < -math.pi:
            rid.angle = 2*math.pi+rid.angle
        if rid.angle < eps and  rid.angle > -eps:
            #if rid.clearway_flag == 'clear'
            rid.pusher_state= STAGE2_REACH
            #else:
            #   rid.vel_msg.linear.x = 0
            #   rid.vel_msg.angular.z = 0
            if rid.message_flag != 'Sent_STAGE2_dir':
                rid.message_flag = 'Sent_STAGE2_dir'
                msg= 'going '
                if rid.goal_state == WEST_WALL or rid.goal_state == EAST_WALL:
                    my_moving_dir = 'y'
                    my_position = rid.odom_data.position.y
                else:
                    my_moving_dir = 'x'
                    my_position = rid.odom_data.position.x
                msg= msg + my_moving_dir + ' ' + str(my_position)
                rid.str_pub.publish(msg)
        else:
            rid.turn_90()
    elif rid.pusher_state == STAGE2_REACH:
        rid.vel_msg.angular.z = 0
        #rid.reach_counter += 1
        if (rid.goal_state == NORTH_WALL or rid.goal_state == SOUTH_WALL) and \
            rid.odom_data.position.x< rid.goal['x']+3*eps and rid.odom_data.position.x > rid.goal['x']-3*eps:
                rid.pusher_state = STAGE3
        elif (rid.goal_state == EAST_WALL or rid.goal_state == WEST_WALL) and \
            rid.odom_data.position.y < rid.goal['y']+3*eps and rid.odom_data.position.y > rid.goal['y']-3*eps:
                rid.pusher_state = STAGE3
                rid.reach_counter = 0
        else:
            #if rid.reach_counter > PUSHER_REACH_THRESHOLD:
            #    rid.reach_counter = 0
            #    rid.pusher_state = STAGE1_TURN
            #else:
            #if sh == 'front':
            #    rid.vel_msg.linear.x = 0
            #    rid.obstacle= True
            #    rid.reach_counter = 0
            #    rid.pusher_state = STAGE2_TURN
            #    rid.prev_w = rid.w
            #else:
            rid.obstacle = False
            rid.move_straight()
    elif rid.pusher_state== STAGE3:
        print(rid.short_low)
        if rid.short_low == 'left':
            rid.vel_msg.angular.z = LEFT
            rid.vel_msg.linear.x = 0
        elif rid.short_low == 'right':
            rid.vel_msg.angular.z = RIGHT
            rid.vel_msg.linear.x = 0
        elif rid.short_low == 'front':
                if rid.reach_counter > PUSHER_STAGE3_THRESHOLD:
                    rid.reach_counter = 0
                    rid.pusher_state = STAGE4_TURN
                else:
                    rid.move_straight()
                    rid.reach_counter +=1
        else:
            rid.move_straight()
    elif rid.pusher_state == STAGE4_TURN:
        my_theta = 2*math.acos(rid.odom_data.orientation.w)
        if rid.odom_data.orientation.z < 0:
            my_theta=2*math.pi - my_theta
        if rid.goal_state== NORTH_WALL or rid.goal_state== SOUTH_WALL:
            if my_theta < NORTH_W or my_theta > SOUTH_W:
                rid.w= EAST_W
            else:
                rid.w= WEST_W
        elif rid.goal_state== EAST_WALL or rid.goal_state== WEST_WALL:
            if my_theta < WEST_W:
                rid.w= NORTH_W
            else:
                rid.w=SOUTH_W
        rid.angle = rid.w - my_theta
        if rid.angle > math.pi:
            rid.angle = rid.angle-2*math.pi
        if rid.angle < -math.pi:
            rid.angle = 2*math.pi+rid.angle
        if rid.angle < eps and  rid.angle > -eps:
            #if rid.clearway_flag == 'clear'
            rid.pusher_state= STAGE4_REACH
        else:
            rid.turn_90()
    elif rid.pusher_state == STAGE4_REACH:
        if min(rid.low_regions['front']) < 0.5:     #if sh== 'front':
            print('Task Done')
            rid.pusher_state = BACKWARD
            rid.vel_msg.angular.z = 0
            rid.vel_msg.linear.x = 0
        else:
            rid.move_straight()
    elif rid.pusher_state == BACKWARD:
        availability[rid.rid] = 1
        rid.step_back()
        if rid.state == DEFAULT:    # this is being resetted at the end of the step_back() function
            print("[ INFO] >> Pusher Robot Finished Task")
            rid.str_pub.publish('finished')
            rid.pusher_state = WAITING
            rid.wait()
    """
    if rid.pusher_state == HELPING:
        if EAST_WALL - abs(rid.goal['x']) < NORTH_WALL - abs(rid.goal['y']):    # move horizontally
            if rid.goal['x'] > 0:       # move east
                rid.w = 0
                rid.goal_state = EAST_WALL
            else:                       # move west
                rid.w = math.pi
                rid.goal_state = WEST_WALL
            if rid.odom_data.position.x > rid.goal['x']-eps*2 and rid.odom_data.position.x < rid.goal['x']+ eps*2:
                rid.pusher_state = TURNING
        else:                                                                   # move vertically
            if rid.goal['y'] > 0:       # move north
                rid.w = math.pi/2
                rid.goal_state = NORTH_WALL
            else:                       # move south
                rid.w= 3*math.pi/2
                rid.goal_state = SOUTH_WALL
            if rid.odom_data.position.y < rid.goal['y']+ eps*2 and rid.odom_data.position.y > rid.goal['y'] - eps*2:
                rid.pusher_state = TURNING
        if rid.message_flag == None:
            rid.message_flag = 'Sent moving dir'
            msg= 'going '
            if rid.goal_state == WEST_WALL or rid.goal_state == EAST_WALL:
                my_moving_dir = 'x'
                my_position = rid.odom_data.position.x
            else:
                my_moving_dir = 'y'
                my_position = rid.odom_data.position.y
            msg= msg + my_moving_dir + ' ' + str(my_position)
            rid.str_pub.publish(msg)
        my_theta = 2*math.acos(rid.odom_data.orientation.w)
        if rid.odom_data.orientation.z < 0:
                my_theta=2*math.pi - my_theta
        if my_theta < rid.w+eps and  my_theta > rid.w-eps:
            if rid.clearway_flag == 'clear':
                rid.move_straight()
            else:
                rid.vel_msg.linear.x = 0
                rid.vel_msg.angular.z = 0
        else:
            rid.turn_90()

    elif rid.pusher_state == TURNING:
        if rid.goal_state == EAST_WALL or rid.goal_state == WEST_WALL:                    # check new vertical direction
            if rid.odom_data.position.y < rid.goal['y']:            # head north
                rid.w = math.pi/2
            else:                                                   # head south
                rid.w = 3*math.pi/2
        else:                                                                       # check new horizontal direction
            if rid.odom_data.position.x < rid.goal['x']:            # head east
                rid.w = 0
            else:                                                   # head west
                rid.w = math.pi

        my_theta = 2*math.acos(rid.odom_data.orientation.w)

        if my_theta < rid.w+eps and my_theta > rid.w-eps:
            rid.pusher_state = PUSHING
        else:
            rid.turn_90()

    elif rid.pusher_state == PUSHING:
        if avg(rid.high_regions['front']) < SHORT_HIGH_DIST + 0.3:  # stop pushing
            rid.wait()
            rid.pusher_state = BACKWARD
        else:
            rid.move_straight()

    elif rid.pusher_state == BACKWARD:
        availability[rid.rid] = 1
        rid.step_back()
        if rid.state == DEFAULT:    # this is being resetted at the end of the step_back() function
            print("[ INFO] >> Pusher Robot Finished Task")
            rid.str_pub.publish('finished')
            rid.pusher_state = WAITING
    else:
        rid.wait()
    """
    if rid.rid != None and rid.pusher_state != None and rid.w != None and my_theta != None and \
        rid.goal['x'] != None and rid.goal['y']!= None and rid.odom_data.position.y != None:

        info_string = "R%d (pusher) %s\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f" %(rid.rid, rid.pusher_state, rid.w, my_theta, rid.goal['x'],rid.goal['y'], rid.odom_data.position.x, rid.odom_data.position.y)
        if info_string != rid.info_string:
            print(info_string)
            rid.info_string = info_string

    rid.vel_pub.publish(rid.vel_msg)

