#!/usr/bin/env python
# -*- coding: utf-8 -*-

from utils import *
from searcher_robot import *
from pusher_robot import *
from main_pusher2 import *
from main_searcher2 import *

def main():

    robots = []

    for i in range(0, NUM_PUSHERS):
        robots.append(PusherRobot(i, range(0 + NUM_PUSHERS, NUM_SEARCHERS + NUM_PUSHERS)))
        distances.append(-1)
        availability.append(1)

    for i in range(0 + NUM_PUSHERS, NUM_SEARCHERS + NUM_PUSHERS):
        robots.append(SearcherRobot(i, range(0, NUM_PUSHERS)))

    #print(robots)

    rospy.init_node('clean_robots', anonymous=True)
    rospy.sleep(0.5)

    while True:
        for rid in robots:
            if rid.__class__ == SearcherRobot:
                searcher_routine(rid)
            else:
                pusher_routine(rid)
            rospy.sleep(0.2)                            # update ratio: 10 Hz



if __name__ == '__main__':
	try:
		main()
	except rospy.ROSInterruptException: pass



'''
TODO LIST for 20/05/2020:
x   modify the print such that less number are used
x   searcher robot needs to continue to work after the pusher robot is done


TODO LIST for 28/05/2020:
x   searcher needs to call for help ONLY if it is near a wall, if it is near another robot it have to recognize it

x   pusher robot needs to go closer to the wall before turning for pushing the can
x   pusher robot needs to step back just a little --> rewrite step_back

x   change the shape/weight of the cans so they will not fall
x   make the robot smaller

-   add 2 more pushers and make the code works
'''