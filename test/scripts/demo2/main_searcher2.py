#!/usr/bin/env python
# -*- coding: utf-8 -*-

from utils import *
from searcher_robot import *

def searcher_routine(rid):

    sl = str(rid.short_low)
    ll = str(rid.long_low)
    sh = str(rid.short_high)
    lh = str(rid.long_high)

    if rid.robot_state == WORKING:
        #if rid.state == BACKWARD:  	#if its stepping back keep steping back
        #    rid.step_back()
        #else:							#else check envoronment
            #print(availability)
        rid.backward = False
        if sum(availability) > 0:   # if there is at least one pusher robot free

            if sh == 'free':

                if lh == 'free':
                    if   sl == 'free' and ll == 'free':
                        rid.search_can()
                    elif sl == 'free' and ll != 'free':
                        if ll == 'front':
                            rid.push_can()
                        else:
                            rid.reach_can()
                    elif sl != 'free' and ll != 'free':
                        if sl == 'front':
                            rid.push_can()
                        else:
                            rid.reach_can()

                elif lh != 'free':
                    if   sl == 'free' and ll == 'free':
                        rid.search_can()
                    elif sl == 'free' and ll != 'free':
                        low_min, high_min = rid.get_mins()
                        if low_min < high_min:
                            if ll == 'front':
                                rid.push_can()
                            else:
                                rid.reach_can()
                        else:
                            rid.search_can()
                    elif sl != 'free' and ll != 'free':
                        if sl == 'front':
                            rid.push_can()
                        else:
                            rid.reach_can()
            else:
                if sl == 'free':
                    if ll == 'free':#or(ll!='free' and ll == lh):# and min(rid.low_regions[ll] == min(rid.high_regions[ll]))):
                        rid.search_can()
                    else:
                        rid.reach_can()#rid.search_can()#
                else:
                    wall_near = rid.is_wall(radius=1.4)
                    if wall_near == None:
                        rid.push_can()          # push_can needs to avoid robots! - almost done
                    else:
                        #low_min, high_min = rid.get_mins()
                        #if high_min == low_min + 0.01:
                            #rid.ask_help()
                        rid.wait()
                        rid.robot_state = STEPBACK
                        rid.ask_help()

        else:
            rid.wait()

    elif rid.robot_state == AVOIDING:
        #if rid.state == BACKWARD:  	#if it's stepping back keep steping back
        #    rid.step_back()
        #else:
        rid.avoid_subroutine()

    elif rid.robot_state == WAITING:
        #if rid.state == BACKWARD:  	#if it's stepping back keep steping back
        #    rid.step_back()
        #else:
        rid.action1 = 'waiting'
        rid.wait()

    elif rid.robot_state == STEPBACK:
        if rid.stepback_counter == 0:
            rid.choose_turning_dir()        # choose a turning dir (the opposite of the wall)
            rid.stepback_counter += 1
        elif rid.stepback_counter > 0 and rid.stepback_counter < 15:     ## THRESHOLD TO MODIFY
            rid.backward = True
            rid.move_straight(turning=True)          # go backward while turning
            rid.stepback_counter += 1
        else:
            rid.backward = False
            rid.stepback_counter = 0            # reset counter
            if rid.is_wall() == None:
                #rid.str_pub.publish("finished")
                #print("[ INFO] >> R{}: I finished pushing".format(rid.rid))
                #availability[rid.rid] = 1
                rid.robot_state = WAITING
            else:
                if rid.stepback_flag == False:
                    rid.stepback_flag = True
                    rid.robot_state = STEPBACK          # change state to WAITING
                else:
                    rid.robot_state = AVOIDING
            rid.wait()                          # wait


    else:
        print("[ ERR] >> state transition error")
        rid.wait()

    if INFO:
        rid.print_info()

    rid.vel_pub.publish(rid.vel_msg)
