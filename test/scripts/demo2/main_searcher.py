#!/usr/bin/env python
# -*- coding: utf-8 -*-

from utils import *
from searcher_robot import *

def searcher_routine(rid):

    sl = rid.short_low
    ll = rid.long_low
    sh = rid.short_high
    lh = rid.long_high

    #info_string = "R{} (searcher) {}\t{}\t{}\t{}\t{}".format(rid.rid, rid.searcher_state, sl, ll, sh, lh)
    info_string = "R%s (searcher) %s\t%s\t%s\t%s\t%s" %(rid.rid, rid.searcher_state, sl, ll, sh, lh)
    if info_string != rid.info_string:
        print(info_string)
        rid.info_string = info_string

    if rid.searcher_state == WORKING:
        if rid.state == BACKWARD:  	#if its stepping back keep steping back
            rid.step_back()
        else:							#else check envoronment
            if sum(availability) > 0:   # if there is at least one pusher robot free
                if sl == 'free' and ll != 'free' and sh == 'free' and lh == 'free' or \
                    sl != 'free' and ll != 'free' and sh == 'free' and lh == 'free' or \
                    sl != 'free' and ll != 'free' and sh == 'free' and lh != 'free':
                    rid.push_can()
                else:
                    if sl == 'free' and ll != 'free' and sh == 'free' and lh != 'free':   #if there's something in the distance
                        if ll == lh and ll != 'front':     # if it's not in front, keep moving
                            rid.move()
                        else:                              # else probably it's a can you can push it
                            rid.push_can()
                    elif sl == 'free' and (ll == 'right' or ll == 'left') and sh == ll and lh == ll:
                        rid.vel_msg.linear.x  = SPEED
                        rid.vel_msg.angular.z = 0
                    elif sl != 'free' and ll != 'free' and sh != 'free' and lh != 'free':
                        low_min  = min(min(rid.low_regions['left']),  min(rid.low_regions['front']),  min(rid.low_regions['right']))
                        high_min = min(min(rid.high_regions['left']), min(rid.high_regions['front']), min(rid.high_regions['right']))

                        # if a low obj is closer than an high AND the low obj is in front
                        if (low_min < high_min - EPS) and min(rid.low_regions['front']) < min(min(rid.low_regions['left']), min(rid.low_regions['right'])):
                            rid.ask_help()
                            rid.step_back()
                            rid.searcher_state = WAITING
                        else:
                            #rid.move()
                            rid.vel_msg.linear.x  = SPEED
                            rid.vel_msg.angular.z = 0
                    else:
                        rid.move()
            else:
                rid.wait()

    elif rid.searcher_state == AVOIDING:
        if rid.avoiding_counter < AVOIDING_THRESHOLD:
            rid.move_straight(backward=True)
            rid.avoiding_counter += 1
        else:
            rid.avoiding_counter = 0
            rid.str_pub.publish("clear")
            print("[ INFO] >> R{}: way cleared".format(rid.rid))
            rid.searcher_state = WAITING

    else:
        if rid.state == BACKWARD:  	#if its stepping back keep steping back
            rid.step_back()
        else:
            rid.wait()

    rid.vel_pub.publish(rid.vel_msg)
