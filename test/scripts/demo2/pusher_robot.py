# -*- coding: utf-8 -*-

from abstract_robot import *

class PusherRobot(Robot):
    """
    Class for storing the pusher robot infos
    """
    def __init__(self, rid, searcher_robots):
        Robot.__init__(self, rid)
        self.goal = {'x' : None, 'y' : None}
        self.goal_state = None
        self.robot_state = WAITING
        self.message_flag = None
        self.clearway_flag = 0
        self.wall = None
        self.obstacle = False
        self.reach_counter= 0
        self.avoiding_flag = 0


        self.msg_flag = False
        self.prev_w = None
        #for i in searcher_robots:
        #    print("[ INFO] >> R{} is subscribing to /m2wr_{}/string topic".format(rid, i))
        #    rospy.Subscriber('/m2wr_'+str(i)+'/string', String, self.pusher_help_callback)

        for i in range(0, NUM_PUSHERS+NUM_SEARCHERS):
            if i != self.rid:
                # print("[ INFO] >> R{} is subscribing to /m2wr_{}/avoidance topic".format(rid, i))
                # rospy.Subscriber('/m2wr_'+str(i)+'/string', String, self.avoid_callback)
                # if i in searcher_robots:
                print("[ INFO] >> R{} is subscribing to /m2wr_{}/string topic".format(rid, i))
                rospy.Subscriber('/m2wr_'+str(i)+'/string', String, self.pusher_help_callback)

        print("        \t>> R{}: push robot ready to work".format(rid))

    def pusher_help_callback(self, data):
        """
        reads the messages published by the searcher robot
        - 'help x y':   published when in need for help, go to (x,y) and push the can to the corner
        - 'clear'   :   reset the clearway flag
        """
        print("[ MSGS] >> R{} received {}".format(self.rid, data))
        tmp = data.data.split()
        '''
        if tmp[0] == 'help':
            if self.robot_state == WAITING:
                goal_x = float(tmp[1])
                goal_y = float(tmp[2])
                my_x = self.odom_data.position.x
                my_y = self.odom_data.position.y
                distances[self.rid] = (goal_x - my_x)**2 + (goal_y - my_y)**2
                print("[ MSGS] >> R{} updated its distance {}".format(self.rid, distances[self.rid]))
            else:
                distances[self.rid] = INFINITE
            rospy.sleep(0.1)
            c = 0
            while min(distances) < 0: # and sum(distances) != NUM_PUSHERS*INFINITE:
                c += 1
                if c %100000 == 0:
                    print("[ WAIT] >> waiting for everyone for publishing their distance... {}".format(distances))
                self.wait()

            chosen = distances.index(min(distances))
            if chosen == self.rid:
                self.goal['x'] = goal_x
                self.goal['y'] = goal_y
                self.robot_state = FACE_WALL
                availability[self.rid] = 0
                print("[ INFO] >> R{} ready to help".format(self.rid))
            else:
                self.robot_state = WAITING
                availability[self.rid] = 1
            distances[self.rid] = -1
        '''

        if tmp[0] == 'help':
            if int(tmp[3]) == self.rid:
                self.goal['x'] = float(tmp[1])
                self.goal['y'] = float(tmp[2])
                self.robot_state = FACE_WALL
                availability[self.rid] = 0
                print("[ INFO] >> R{} ready to help".format(self.rid))
            else:
                pass

        elif tmp[0] == 'going':
            if tmp[1] == 'x':
                if self.odom_data.position.x > float(tmp[2]) - EPS and self.odom_data.position.x < float(tmp[2]) + EPS:
                    if self.odom_data.position.x < 0:
                        self.w = EAST_W
                    else:
                        self.w = WEST_W
                    print("[ INFO] >> R{} will move because he is at x {}".format(self.rid, self.odom_data.position.x))
                    self.robot_state = AVOIDING
                else:
                    self.str_pub.publish("clear")
                    print("[ INFO] >> R{}: way already cleared".format(self.rid))

            else:
                if self.odom_data.position.y > float(tmp[2]) - EPS and self.odom_data.position.y < float(tmp[2]) + EPS:
                    if self.odom_data.position.y < 0:
                        self.w = NORTH_W
                    else:
                        self.w = SOUTH_W
                    print("[ INFO] >> R{} will move because he is at y {}".format(self.rid, self.odom_data.position.y))
                    self.robot_state = AVOIDING
                else:
                    self.str_pub.publish("clear")
                    print("[ INFO] >> R{}: way already cleared".format(self.rid))

        elif tmp[0] == 'clear':
            if self.robot_state == FACE_CAN or self.robot_state == FACE_WALL:
                self.clearway_flag += 1

        else:
            print("[ ERR] >> pusher_help_callback: state communication error : data = {}".format(data))


    def choose_wall(self):
        """
        Sets `self.goal_state` accordingly to the position of the can
        """
        self.action1 = "choosing wall"
        if EAST_WALL - abs(self.goal['x']) < NORTH_WALL - abs(self.goal['y']):
            if self.goal['x'] > 0:
                self.w = EAST_W
                self.goal_state = EAST_WALL
            else:
                self.w = WEST_W
                self.goal_state = WEST_WALL
        else:
            if self.goal['y'] > 0:
                self.w = NORTH_W
                self.goal_state = NORTH_WALL
            else:
                self.w = SOUTH_W
                self.goal_state = SOUTH_WALL

    def choose_corner(self):
        """
        Sets `self.w` accordingly to the position of the chosen corner
        """
        self.action1 = "choosing corner"
        if self.goal_state == EAST_WALL or self.goal_state == WEST_WALL:                    # check new vertical direction
            if self.odom_data.position.y < self.goal['y']:            # head north
                self.goal_state = NORTH_WALL
                self.w = NORTH_W
            else:                                                   # head south
                self.goal_state = SOUTH_WALL
                self.w = SOUTH_W
        else:                                                                       # check new horizontal direction
            if self.odom_data.position.x < self.goal['x']:            # head east
                self.goal_state = EAST_WALL
                self.w = EAST_W
            else:                                                   # head west
                self.goal_state = WEST_WALL
                self.w = WEST_W

    def send_moving_dir(self):
        """
        Sends the moving direction to the other robots
        """
        self.clearway_flag = 0
        msg = "going "
        if self.goal_state == WEST_WALL or self.goal_state == EAST_WALL:
            my_moving_dir = 'y '
            my_position = self.odom_data.position.y
        else:
            my_moving_dir = 'x '
            my_position = self.odom_data.position.x
        msg = msg + my_moving_dir + str(my_position)
        self.str_pub.publish(msg)
        print("[ INFO] >> R{} asks anyone in {} {} to move".format(self.rid, my_moving_dir, my_position))


    def face_wall(self):
        my_theta = self.compute_theta()
        self.angle = self.w - my_theta
        if self.angle > math.pi:
            self.angle -=  2*math.pi
        elif self.angle < -math.pi:
            self.angle += 2*math.pi


    def compute_theta(self):
        my_theta = 2*math.acos(self.odom_data.orientation.w)
        if self.odom_data.orientation.z < 0:
            my_theta = 2*math.pi - my_theta
        return my_theta


    def choose_turning_dir(self):
        """
        Choose the turning dir to make the robot avoid the wall while going backward
        """
        self.action1 = "choosing turn dir"
        if self.short_high == 'left':
            self.action2 = "left!"
            self.turning_dir = LEFT
        elif self.short_high == 'right':
            self.action2 = "right!"
            self.turning_dir = RIGHT
        else:
            self.turning_dir = None

    def turn_90(self):
        self.action1 = "turn 90"
        self.vel_msg.linear.x = 0
        self.face_wall()
        if self.angle > 0:
            self.action2 = "turning left"
            self.vel_msg.angular.z = LEFT# Left makes my_theta increase
        else:
            self.action2 = "turning right"
            self.vel_msg.angular.z = RIGHT


    def avoid_subroutine(self):
        """
        Makes the robot avoid another robot and go away from the walls
        """
        if self.avoiding_flag == 5:
            self.avoiding_flag = 0
            if self.stepback_flag == True:
                self.stepback_flag = False
                self.str_pub.publish("finished")
                print("[ INFO] >> R{}: I finished pushing".format(self.rid))
                availability[self.rid] = 1
            self.str_pub.publish("clear")
            self.robot_state = WAITING
        else:
            if self.avoiding_counter == 0:
                self.choose_avoiding_direction()
                self.avoiding_counter = 1
                self.wait()
            elif self.avoiding_counter > 0 and self.avoiding_counter < AVOIDING_THRESHOLD:
                self.move_straight(turning=True)
                self.avoiding_counter += 1
            else:
                self.backward = False
                self.turning_dir = None
                self.avoiding_counter = 0
                if self.is_wall() == None:
                    if self.stepback_flag == True:
                        self.stepback_flag = False
                        self.str_pub.publish("finished")
                        print("[ INFO] >> R{}: I finished pushing".format(self.rid))
                        availability[self.rid] = 1
                    self.str_pub.publish("clear")
                    print("[ INFO] >> R{}: way cleared".format(self.rid))
                    self.robot_state = WAITING
                else:
                    self.avoiding_flag += 1
                    self.robot_state = AVOIDING


    def print_info(self):
        sh = self.short_high
        sl = self.short_low

        if self.is_wall() == NORTH_WALL:
            wall = "North"
        elif self.is_wall() == SOUTH_WALL:
            wall = "South"
        elif self.is_wall() == EAST_WALL:
            wall = "East"
        elif self.is_wall() == WEST_WALL:
            wall = "West"
        else:
            wall = "None"

        s = "R%s (pusher) %s: %s\t(%s)\tgoal: %s\t clearway flag:%s\t%s\t%s\t%s" %(self.rid, self.robot_state, self.action1, self.action2, self.goal_state, self.clearway_flag, sl, sh, wall)

        if s != self.info_string:
            print(s)
            self.info_string = s
