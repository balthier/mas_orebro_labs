#!/usr/bin/env python
# -*- coding: utf-8 -*-

from utils import *
from pusher_robot import *


def pusher_routine(rid):
    sl = rid.short_low
    ll = rid.long_low
    sh = rid.short_high
    lh = rid.long_high
    wall = rid.is_wall()
    eps = 0.15 ##########################################################################################################

    if rid.robot_state == WAITING:
        rid.action1 = "waiting"
        rid.wait()

    elif rid.robot_state == FACE_WALL:
        if rid.msg_flag == False:               # if I didn't send the message:
            rid.choose_wall()                       # which wall should I reach?
            rid.face_wall()                         # set the angle you should turn to
            rid.send_moving_dir()                   # send the message saying my current moving direction
            rid.msg_flag = True
            rid.wait()
        else:
            my_theta = rid.compute_theta()

            if my_theta > rid.w + eps  or  my_theta < rid.w - eps:  # if you're not facing the wall:  # WHERE TO SET w ???????
                rid.turn_90()   # turn until you face the wall
            else:
                if rid.clearway_flag == 2:
                    rid.clearway_flag = 0       # reset the flag "way clear"
                    rid.msg_flag = False            # reset the flag "message sent"
                    rid.robot_state = REACH_WALL   # change state to REACH_WALL

                rid.wait()

    elif rid.robot_state == REACH_WALL:

        if rid.is_wall(radius=0.4) != rid.goal_state:

        #if ((avg(rid.high_regions['front']) > 0.5 and min(rid.low_regions['front']) > 0.5) or \
        #   (avg(rid.high_regions['front']) > 0.5+0.24 and min(rid.low_regions['front']) < 0.25)) and\
        #    rid.is_wall() != rid.goal_state:
            # if you are not close to the wall (how much close?):
            rid.move_straight()                 # go straight
        else:
            rid.robot_state = FACE_CAN         # change state to FACE_CAN
            rid.wait()                          # wait

    elif rid.robot_state == FACE_CAN:
        if rid.msg_flag == False:               # if I didn't send the message:
            rid.choose_corner()                     # which corner should I reach?
            rid.send_moving_dir()                   # send the message saying my current moving direction
            rid.msg_flag = True
            rid.wait()
        else:
            my_theta = rid.compute_theta()

            if my_theta > rid.w + eps  or  my_theta < rid.w - eps:  # if I'm not facing the corner:  # WHERE TO SET w ???????
                rid.turn_90()   # turn until I face the corner
            else:
                if rid.clearway_flag == 2:
                    rid.clearway_flag = 0       # reset the flag "way clear"
                    rid.msg_flag = False            # reset the flag "message sent"
                    rid.robot_state = PUSH_CAN     # change state to PUSH_CAN

                rid.wait()

    elif rid.robot_state == PUSH_CAN:
        if avg(rid.high_regions['front']) > SHORT_HIGH_DIST + 2*eps: # if i'm not close to the corner
            rid.move_straight()             # go straight
        else:
            rid.robot_state = STEPBACK     # change state to STEPBACK
            rid.wait()                      # wait

    elif rid.robot_state == STEPBACK:
        if rid.stepback_counter == 0:
            rid.choose_turning_dir()        # choose a turning dir (the opposite of the wall)
            rid.stepback_counter += 1
        elif rid.stepback_counter > 0 and rid.stepback_counter < 10:     ## THRESHOLD TO MODIFY
            rid.backward = True
            rid.move_straight(turning=True)          # go backward while turning
            rid.stepback_counter += 1
        else:
            rid.backward = False
            rid.stepback_counter = 0            # reset counter
            if rid.is_wall() == None:
                rid.str_pub.publish("finished")
                print("[ INFO] >> R{}: I finished pushing".format(rid.rid))
                availability[rid.rid] = 1
                rid.str_pub.publish("clear")
                rid.robot_state = WAITING
            else:
                if rid.stepback_flag == False:
                    rid.stepback_flag = True
                    rid.robot_state = STEPBACK          # change state to WAITING
                else:
                    rid.robot_state = AVOIDING
            rid.wait()                          # wait

    elif rid.robot_state == AVOIDING:
        rid.avoid_subroutine()

    else:
        rid.wait()
        print("[ ERR] >> R{}: state not defined.".format(rid.rid))

    if INFO:
        rid.print_info()
    rid.vel_pub.publish(rid.vel_msg)
