To install it, the `test` and `m2wr_description` folders must be moved to the workspace and compiled as ROS Packages.
Once the packages are compiled, with ROS and Gazebo installed, the project should run correctly.

## Download & install
To download the GitLab repository use the following command:
`git clone https://gitlab.com/balthier/mas_orebro_labs`

To compile the folders as ROS Packages, move them to the workspace and compile them, in case of doubt check the ROS tutorials: http://wiki.ros.org/ROS/Tutorials

## Run demo 2
To run the Project with three robots:
Use the following command to start gazebo:
`roslaunch m2wr_description/launch/start_demo2.launch`

And on a new terminal start the software with the command:
`rosrun test main.py`

## Run demo 3
To run the Project with four robots:
Use the following command to start gazebo:
`roslaunch m2wr_description/launch/start_demo3.launch`

And on a new terminal start the software with the command:
`rosrun test main2.py`

Note that in order to correctly change the number of robots, the parameter NUM_PUSHERS in the util.py file must be changed accordingly.


## Printed Information:

Each robot will print its own state each time the state changes, and they will also print the messages that are being sent between robots.
The parameters that are being printed by the searcher robot are:
self.rid, self.robot_state, self.action1, self.action2, sl, ll, sh, lh, wall
The parameters printed by the pusher robot are:
self.rid, self.robot_state, self.action1, self.action2, self.goal_state, self.clearway_flag, sl, sh, wall
