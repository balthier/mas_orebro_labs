import matplotlib
import matplotlib.pyplot as plt
import numpy as np

def speedup(data1, data2):
    res = []
    for i in range(0, len(data1)):
        res.append(data1[i]/data2[i])
    print(res, np.mean(res))
    return np.mean(res)

demo2_3OBJ_timings = [33, 20, 25, 14, 30, 21]
demo2_2OBJ_timings = [46, 43, 37, 48, 30, 34]
demo2_1OBJ_timings = [55, 51, 40, 57, 36, 50]
demo2_0OBJ_timings = [75, 58, 41, 62, 39, 60]

demo3_3OBJ_timings = [5, 2, 7, 10, 11, 5]
demo3_2OBJ_timings = [6, 5, 15, 13, 11, 6]
demo3_1OBJ_timings = [11, 6, 20, 19, 12, 6]
demo3_0OBJ_timings = [38, 16, 25, 20, 17, 7]

demo2_data = [demo2_3OBJ_timings, demo2_2OBJ_timings, demo2_1OBJ_timings, demo2_0OBJ_timings]
demo2_sample = [demo2_3OBJ_timings[1], demo2_2OBJ_timings[1], demo2_1OBJ_timings[1], demo2_0OBJ_timings[1]]
demo3_data = [demo3_3OBJ_timings, demo3_2OBJ_timings, demo3_1OBJ_timings, demo3_0OBJ_timings]
demo2_sample = [demo3_3OBJ_timings[3], demo3_2OBJ_timings[3], demo3_1OBJ_timings[3], demo3_0OBJ_timings[3]]

demo2_means = []
demo3_means = []
demo2_var = []
demo3_var = []

for i in range(0, len(demo2_data)):
    demo2_means.append(np.mean(demo2_data[i]))
    demo2_var.append(np.std(demo2_data[i]))

print()
for i in range(0, len(demo3_data)):
    demo3_means.append(np.mean(demo3_data[i]))
    demo3_var.append(np.std(demo3_data[i]))

print("average speedups")
speedup(demo2_means, demo3_means)

width = 0.4
ind = np.arange(4)
names = ["3 cans", "2 cans", "1 can", "0 cans"]
p1 = plt.bar(ind, demo2_means, width, yerr=demo2_var, label='Short Range')
p2 = plt.bar(ind+width, demo3_means, width, bottom=0, yerr=demo3_var, label='Long Range')
plt.title("Short Range VS Long Range Algorithm Cleaning Performance")
plt.ylabel("Average time needed (sec)")
plt.xticks(ind+width/2, names)
plt.grid()
plt.legend()
plt.show()
